library(httr)
library(servr)

port <- random_port()
url = "localhost:4242/services"
body = paste('{"service": "intent", "port":',port,'}')

POST(url = url, body = body, content_type_json())

print("registered to services-registry")

