package org.fap.guys.studio.services.registry;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
public class ServicesController {

    private final Logger logger = Logger.getLogger(ServicesController.class.toString());

    private List<Service> services;
    private final Random random;

    public ServicesController() {
        this.services = new ArrayList<>();

        random = new Random();
    }

    @CrossOrigin
    @RequestMapping(value = "/services")
    public List<Service> getServices() {
        return this.services;
    }

    @CrossOrigin
    @RequestMapping(value = "/services", method = RequestMethod.POST)
    public ResponseEntity<String> registerService(@RequestBody Service service) {
        logger.info(service.toString());
        this.services.add(service);
        logger.info("Registered service " + service.getService().toString() + " on port " + service.getPort());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    
    @CrossOrigin
    @RequestMapping("/services/{service}")
    public Service service(@PathVariable("service") Services service) {
        logger.info("Getting services for service " + service.name());
        List<Service> servicesList = this.services.stream().filter(a -> a.getService().equals(service)).collect(Collectors.toList());
        return servicesList.get(random.nextInt(servicesList.size()));
    }
}
