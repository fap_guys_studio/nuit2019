package org.fap.guys.studio.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class ResourcesController {

    private Resource    deplacements,
                        habitat,
                        budget,
                        sorties,
                        ecologie,
                        sante,
                        handicap,
                        benevolat,
                        sexualite,
                        international,
                        diy,
                        vacances,
                        organisation,
                        reseauxsociaux,
                        jobetudiant,
                        orientation;

    public ResourcesController() {
        this.deplacements = new Resource("deplacements", "Déplacements.");
        this.habitat = new Resource("habitat","Habitat");
        this.budget = new Resource("budget","budget");
        this.sorties = new Resource("sorties","sorties");
        this.ecologie = new Resource("ecologie","ecologie");
        this.sante = new Resource("sante","sante");
        this.handicap = new Resource("handicap","handicap");
        this.benevolat = new Resource("benevolat","benevolat");
        this.sexualite = new Resource("sexualite","sexualite");
        this.international = new Resource("international","international");
        this.diy = new Resource("diy","diy");
        this.vacances = new Resource("vacances","vacances");
        this.organisation = new Resource("organisation","organisation");
        this.reseauxsociaux = new Resource("reseauxsociaux","reseauxsociaux");
        this.jobetudiant = new Resource("jobetudiant","jobetudiant");
        this.orientation = new Resource("orientation","orientation");
    }

    @CrossOrigin
    @RequestMapping("/resources")
    public List<Resource> mapDeSesGrandsMorts(){
       List<Resource> resources = new ArrayList<>();
        resources.add(new Resource("deplacements", "Déplacements."));
        resources.add(new Resource("habitat","Habitat"));
        resources.add(new Resource("budget","budget"));
        resources.add(new Resource("sorties","sorties"));
        resources.add(new Resource("sante","sante"));
        resources.add(new Resource("ecologie","ecologie"));
        resources.add(new Resource("handicap","handicap"));
        resources.add(new Resource("benevolat","benevolat"));
        resources.add(new Resource("sexualite","sexualite"));
        resources.add(new Resource("international","international"));
        resources.add(new Resource("diy","diy"));
        resources.add(new Resource("vacances","vacances"));
        resources.add(new Resource("organisation","organisation"));
        resources.add(new Resource("reseauxsociaux","reseauxsociaux"));
        resources.add(new Resource("jobetudiant","jobetudiant"));
        resources.add(new Resource("orientation","orientation"));

        return resources;
    }

    @RequestMapping("/resources/{resource}")
    public Resource resource(@PathVariable("resource") String resource) {
        return deplacements;
    }

}
