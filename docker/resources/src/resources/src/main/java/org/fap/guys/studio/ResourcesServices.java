package org.fap.guys.studio;

import org.fap.guys.studio.services.registry.Service;
import org.fap.guys.studio.services.registry.Services;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.util.SocketUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;

@SpringBootApplication
public class ResourcesServices {

    private static final String SERVICES_REGISTY_URL = "http://localhost:4242/services";

    public static void main(String[] args) throws IOException {
        int port = SocketUtils.findAvailableTcpPort();
        System.out.println("Starting ResourcesServices on port " + port);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(SERVICES_REGISTY_URL, new Service(port, Services.resources), ResponseEntity.class);

        SpringApplication app = new SpringApplication(ResourcesServices.class);
        app.setDefaultProperties(
                Collections.singletonMap("server.port", port)
        );
        app.run();
    }
}
