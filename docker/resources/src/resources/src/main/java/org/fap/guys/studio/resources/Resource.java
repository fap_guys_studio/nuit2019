package org.fap.guys.studio.resources;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Resource {

    private String title;
    private String description;

}
