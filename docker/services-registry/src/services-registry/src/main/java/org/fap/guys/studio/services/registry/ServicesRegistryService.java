package org.fap.guys.studio.services.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class ServicesRegistryService {

    private static final int PORT = 4242;

    public static void main(String[] args) {
        System.out.println("Starting ServicesRegistry on port " + PORT);

        SpringApplication app = new SpringApplication(ServicesRegistryService.class);
        app.setDefaultProperties(
                Collections.singletonMap("server.port", PORT)
        );
        app.run();
    }
}
