#/bin/sh
rm docker/services-registry/src -rf
rm docker/resources/src -rf
rm docker/intent/src -rf
rm docker/web/src -rf

cp backend docker/services-registry/src -r
cp backend docker/resources/src -r
cp backend/intent docker/intent/src -r
cp frontend docker/web/src -r