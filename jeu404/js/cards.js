const cards = {
  'cards': [{
    'value': 'Je m\'achète ce nouveau mac ?',
    'option': {
      'left': {
        'money': 0,
        'academic': 0,
        'social': 10,
        'health': -10
      },
      'right': {
        'money': -30,
        'academic': 10,
        'social': 5,
        'health': 0
      },
      'random': {
        'money': {
          'min': -5,
          'max': 5
        },
        'academic': {
          'min': -5,
          'max': 5
        },
        'social': {
          'min': -5,
          'max': 5
        },
        'health': {
          'min': -5,
          'max': 5
        }
      }
    }
  },
    {
      'value': 'Soirée ce soir ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 5,
          'social': -5,
          'health': 2
        },
        'right': {
          'money': -8,
          'academic': -4,
          'social': 10,
          'health': -5
        },
        'random': {
          'money': {
            'min': -10,
            'max': 5
          },
          'academic': {
            'min': -6,
            'max': 2
          },
          'social': {
            'min': 0,
            'max': 8
          },
          'health': {
            'min': -7,
            'max': 5
          }
        }
      }
    },
    {
      'value': 'Deliveroo ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 5,
          'social': -1,
          'health': 5
        },
        'right': {
          'money': -5,
          'academic': 0,
          'social': 1,
          'health': -2
        },
        'random': {
          'money': {
            'min': -5,
            'max': 5
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -5,
            'max': 5
          }
        }
      }
    },
    {
      'value': 'Je reste sur une défaite ?',
      'option': {
        'left': {
          'money': 0,
          'academic': -5,
          'social': 0,
          'health': -5
        },
        'right': {
          'money': 0,
          'academic': 5,
          'social': 5,
          'health': 5
        },
        'random': {
          'money': {
            'min': 0,
            'max': 0
          },
          'academic': {
            'min': -5,
            'max': 3
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -2,
            'max': 2
          }
        }
      }
    },
    {
      'value': 'Est-ce que je vais en amphi aujourd’hui ?',
      'option': {
        'left': {
          'money': 0,
          'academic': -8,
          'social': 2,
          'health': 0
        },
        'right': {
          'money': 0,
          'academic': 5,
          'social': -1,
          'health': -1
        },
        'random': {
          'money': {
            'min': -5,
            'max': 5
          },
          'academic': {
            'min': -2,
            'max': 7
          },
          'social': {
            'min': -2,
            'max': 2
          },
          'health': {
            'min': -5,
            'max': 5
          }
        }
      }
    },
    {
      'value': 'Un petit dernier épisode ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 2,
          'social': 0,
          'health': 5
        },
        'right': {
          'money': 0,
          'academic': -2,
          'social': 1,
          'health': -5
        },
        'random': {
          'money': {
            'min': -5,
            'max': 5
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -5,
            'max': 5
          }
        }
      }
    },
    {
      'value': 'Est-ce que je révise mes cours ?',
      'option': {
        'left': {
          'money': 0,
          'academic': -5,
          'social': 5,
          'health': 0
        },
        'right': {
          'money': 0,
          'academic': 5,
          'social': -5,
          'health': -5
        },
        'random': {
          'money': {
            'min': -2,
            'max': 2
          },
          'academic': {
            'min': -10,
            'max': 10
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -5,
            'max': 5
          }
        }
      }
    },
    {
      'value': 'Je vais à la salle ce soir ? ',
      'option': {
        'left': {
          'money': 0,
          'academic': 2,
          'social': -2,
          'health': -2
        },
        'right': {
          'money': -5,
          'academic': 0,
          'social': 2,
          'health': 5
        },
        'random': {
          'money': {
            'min': -5,
            'max': 5
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -5,
            'max': 5
          }
        }
      }
    },
    {
      'value': 'Est-ce que je rentre chez mes parents ce week-end ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 5,
          'social': -3,
          'health': 0
        },
        'right': {
          'money': -5,
          'academic': -2,
          'social': 5,
          'health': -1
        },
        'random': {
          'money': {
            'min': -5,
            'max': 5
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -2,
            'max': 2
          }
        }
      }
    },
    {
      'value': 'Est-ce que je cuisine healthy ce soir ?',
      'option': {
        'left': {
          'money': -2,
          'academic': 2,
          'social': 0,
          'health': -8
        },
        'right': {
          'money': -5,
          'academic': 0,
          'social': 0,
          'health': 8
        },
        'random': {
          'money': {
            'min': -9,
            'max': 7
          },
          'academic': {
            'min': -3,
            'max': 3
          },
          'social': {
            'min': -2,
            'max': 2
          },
          'health': {
            'min': -10,
            'max': 10
          }
        }
      }
    },
    {
      'value': 'Est-ce que je me couche tôt ce soir ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 1,
          'social': 1,
          'health': -5
        },
        'right': {
          'money': 0,
          'academic': 3,
          'social': 2,
          'health': 5
        },
        'random': {
          'money': {
            'min': 0,
            'max': 0
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -2,
            'max': 2
          },
          'health': {
            'min': -8,
            'max': 2
          }
        }
      }
    },
    {
      'value': 'Est-ce que je déjeune ce matin ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 0,
          'social': 0,
          'health': -2
        },
        'right': {
          'money': 0,
          'academic': 0,
          'social': 0,
          'health': 2
        },
        'random': {
          'money': {
            'min': -5,
            'max': 5
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -5,
            'max': 5
          }
        }
      }
    },
    {
      'value': 'Allez je dors 10 min de plus ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 0,
          'social': 0,
          'health': 2
        },
        'right': {
          'money': 0,
          'academic': 0,
          'social': 0,
          'health': 5
        },
        'random': {
          'money': {
            'min': -5,
            'max': 5
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -5,
            'max': 5
          }
        }
      }
    },
    {
      'value': 'Est-ce que je vais chez biocoop ?',
      'option': {
        'left': {
          'money': -1,
          'academic': 0,
          'social': 0,
          'health': -2
        },
        'right': {
          'money': -5,
          'academic': 0,
          'social': 2,
          'health': 5
        },
        'random': {
          'money': {
            'min': -15,
            'max': 5
          },
          'academic': {
            'min': -6,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 8
          },
          'health': {
            'min': -5,
            'max': 9
          }
        }
      }
    },
    {
      'value': 'Est-ce que je vais à la caf ?',
      'option': {
        'left': {
          'money': -3,
          'academic': 2,
          'social': 2,
          'health': 5
        },
        'right': {
          'money': 10,
          'academic': -5,
          'social': -5,
          'health': -5
        },
        'random': {
          'money': {
            'min': 0,
            'max': 15
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -2,
            'max': 2
          },
          'health': {
            'min': -10,
            'max': 2
          }
        }
      }
    },
    {
      'value': 'Est ce que je vais manger chez mamie ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 2,
          'social': -7,
          'health': -2
        },
        'right': {
          'money': 5,
          'academic': 2,
          'social': 6,
          'health': -2
        },
        'random': {
          'money': {
            'min': -5,
            'max': 5
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -10,
            'max': 10
          }
        }
      }
    },
    {
      'value': 'Alors un petit footing ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 2,
          'social': -2,
          'health': -2
        },
        'right': {
          'money': 0,
          'academic': 0,
          'social': 2,
          'health': 5
        },
        'random': {
          'money': {
            'min': -5,
            'max': 5
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -5,
            'max': 5
          }
        }
      }
    },
    {
      'value': 'Je participe à la soirée d\'integration ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 2,
          'social': -2,
          'health': 0
        },
        'right': {
          'money': -5,
          'academic': -1,
          'social': 5,
          'health': -5
        },
        'random': {
          'money': {
            'min': -10,
            'max': 2
          },
          'academic': {
            'min': -5,
            'max': 5
          },
          'social': {
            'min': -5,
            'max': 10
          },
          'health': {
            'min': -15,
            'max': 0
          }
        }
      }
    },
    {
      'value': 'Je participe à la nuit de l\'info ?',
      'option': {
        'left': {
          'money': 0,
          'academic': -7,
          'social': -5,
          'health': 2
        },
        'right': {
          'money': 0,
          'academic': 5,
          'social': 8,
          'health': -5
        },
        'random': {
          'money': {
            'min': -2,
            'max': 3
          },
          'academic': {
            'min': -2,
            'max': 10
          },
          'social': {
            'min': -2,
            'max': 5
          },
          'health': {
            'min': -10,
            'max': 2
          }
        }
      }
    },
    {
      'value': 'Je revends sur wish ce jean jamais porté ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 6,
          'social': -2,
          'health': 1
        },
        'right': {
          'money': 5,
          'academic': 0,
          'social': 1,
          'health': -3
        },
        'random': {
          'money': {
            'min': -5,
            'max': 8
          },
          'academic': {
            'min': -2,
            'max': 3
          },
          'social': {
            'min': -5,
            'max': 5
          },
          'health': {
            'min': -2,
            'max': 2
          }
        }
      }
    },
    {
      'value': 'Je vais tondre le gazon du voisin ?',
      'option': {
        'left': {
          'money': 0,
          'academic': 0,
          'social': 2,
          'health': 0
        },
        'right': {
          'money': 5,
          'academic': 0,
          'social': 0,
          'health': 5
        },
        'random': {
          'money': {
            'min': -5,
            'max': 10
          },
          'academic': {
            'min': -5,
            'max': 2
          },
          'social': {
            'min': -2,
            'max': 2
          },
          'health': {
            'min': -10,
            'max': 3
          }
        }
      }
    }]
}.cards
