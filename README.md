# Nuit de l'info 2019

Notre projet consiste en chat bot en front ecrit en VueJS.
Avec un backend décrit un peu plus bas.

L'idée de notre chat bot est qu'il soit le plus interactif, que ce ne soit pas juste du texte et des liens.
Mais bien qu'il puisse proposer des tutos, des idées, ...
Evidement on n'a pas eu le temps ;).

### Idées d'utilsiation du bot

- Déplacements
- Habitat
- Petit budget
- Sorties
- Ecologie pratique
- Santé
- Handicaps
- Engagement/volontariat
- Genre / sexualité
- International
- Faire soi-même
- Vacances
- Organisation
- Réseaux Sociaux
- Job étudiant
- Orientation

## Les defis :

- Le défi: ATOS - 404 GAME FOUND
- Le défi: Dockerisation
- Le défi: Micro-Services pour Maxi défi !
- Le défi: Cache-cache avec le Chat Botté

## Jeu 404
Lire le readme associé dans le dossier dédié

## Les microservices

3 Microservices :
Services-Registry : registre des microservices, définis par leur nom et leur port, en Java
Resources : répertoire de documents, en Java
Intents : évaluateur d'intention, en R
Déplacements
Habitat
Petit budget
Sorties
Ecologie pratique
Santé
Handicaps
Engagement/volontariat
Genre / sexualité
International
Faire soi-même
Vacances
Organisation
Réseaux Sociaux
Job étudiant
Orientation
Déploiement backend

`cd backend/`
`mvn clean install`
`mvn mvn spring-boot:run` dans backend/services-registry/ puis dans backend/resources (dans 2 terminaux différents ou en tâche de fond avec &)

installer R : `sudo apt install r-base`
`sudo apt install libcurl4-openssl-dev`
puis dans R :
`install.packages("httr")`
`install.packages("plumber")`
`install.packages("servr")`

cd backend/intent/
R -f IntentService.R

Pour tester : 
Liste des microservices :
curl localhost:4242/services

Liste des resources
curl localhost:[port resources]/resources

Évaluation d'une intention
curl localhost:[port intents]/intent

Déployer avec docker :
./pre-build.sh
docker-compose build
docker-compose up
